<?php
// $Id $

/**
 * @file
 * Author: Sven Decabooter
 * Provides an easy interface to translate menu items
 * Parts of the code inspired by i18nmenu module by Jose A. Reyero
 */

/**
 * Implementation of hook_help().
 */
function menutranslation_help($section = '') {
  $output = '';
  switch ($section) {
    case "admin/help#menutranslation" :
      $output = t("Provides an easy interface to translate menu items");
      break;
  }
  return $output;
}

/**
 * Valid permissions for this module
 * @return array An array of valid permissions for the module
 */
function menutranslation_perm() {
  return array('translate menus', 'administer menutranslation');
}

/**
 * Implementation of hook_menu().
 */
function menutranslation_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/menutranslation',
      'title' => t('Menu Translation'),
      'description' => t('Provides an easy interface to translate menu items'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'menutranslation_admin',
      'access' => user_access('administer menutranslation'),
      'type' => MENU_NORMAL_ITEM
    );
    $items[] = array(
      'path' => 'admin/build/menu/menutranslation',
      'title' => t('Menu Translation'),
      'callback' => 'menutranslation_page',
      'access' => user_access('translate menus'),
      'type' => MENU_LOCAL_TASK
    );
  } 
  else {
    menutranslation_translate_menus();
  }
  return $items;
}

function menutranslation_admin() {
  $form = array();
  $menu_options = menu_get_root_menus();

  $form['menutranslation_menu'] = array(
  '#type' => 'select',
  '#title' => t('Menu(s) to translate'),
  '#default_value' => variable_get('menutranslation_menu', 0),
  '#options' => $menu_options,
  '#multiple' => true,
  '#size' => count($menu_options),
  '#description' => t('Select which menu(s) should be translatable. Hold Shift key to select multiple items.')
  );	

  menu_rebuild();

  return system_settings_form($form);
}

function menutranslation_page($mid = 0, $lang = NULL) {  
  $output = '';
  if ($mid != 0 && $lang == NULL) {
    // show add/edit form
    $output .= _menutranslation_lang_list($mid);
  } 
  elseif ($mid != 0 && $lang != NULL) {
    // show translation form
    $output .= _menutranslation_translate($mid, $lang);
  }
  else {
    // show menu item list  
    if ($selected_menus = variable_get('menutranslation_menu', array())) {
      foreach ($selected_menus as $mid) {
        $output .= theme('menutranslation_menutable',$mid);
      }
    } 
    else {
      drupal_set_message('Please select a menu that you want to translate in the <a href="/admin/settings/menutranslation">menutranslation settings</a>.', 'error');
    }     
  }
  return $output;
}

function theme_menutranslation_menutable($mid) {
  $header = array(t('Menu item'), t('Operations'));
  $menu = menu_get_item($mid);
  $rows = array();
  _menutranslation_render_rows($rows, $menu, 0);
  $table = theme('table', $header, $rows);
  $output .= theme('box', $menu['visible'][$mid]['title'], $table);
  return $output;
}

function _menutranslation_render_rows(&$rows, $menu = NULL, $depth = 0) {
  $curr_depth = $depth;
  if (isset($menu['children'])) {
    $depth++;
    foreach ($menu['children'] as $mid) {
      $menu = menu_get_item($mid);
      $rows[] = array(array('data' => theme('menutranslation_show_depth',$curr_depth) . $menu['title']), array('data' => l(t('add/edit translation'), 'admin/build/menu/menutranslation/'. $mid)));
      _menutranslation_render_rows($rows, $menu, $depth);
    }
  }  
}

function theme_menutranslation_show_depth($depth) {
  $depthstring = '';
  if ($depth > 0) {
    for ($i=1;$i<$depth;$i++) {
      $depthstring .= '&nbsp;&nbsp;';
    }
    $depthstring .= '-&nbsp;';
  }
  return $depthstring;
}

function _menutranslation_lang_list($mid) {
  if (is_numeric($mid)) {
    $menu_item = menu_get_item($mid);
    $sql_title =  "SELECT title FROM {menu} WHERE mid = '%d'";
    $result = db_fetch_array(db_query($sql_title, $mid));
    $title = $result['title'];
    return theme('menutranslation_translationtable',$mid,$title);
  }
}

function theme_menutranslation_translationtable($mid,$title){
  // set breadcrumb
  drupal_set_breadcrumb(array(l(t('Home'), NULL), l(t('Administer'), 'admin'), l(t('Menutranslation'), 'admin/build/menu/menutranslation')));
  // set title
  drupal_set_title(t('Translation for menu item') .'<em> '. $title .'</em>');
  // get languages
  $languages = _i18n_locale_supported_languages();
  $translation_arr = array();
  $header = array(t('Language'), t('Translation'), t('Operations'));
  foreach ($languages as $lang => $lang_name) {
    $translation = _menutranslation_get_translation($mid,$lang);
    $label = ($translation != t('n/a'))?t('edit translation'):t('create translation');
    $rows[] = array(array('data' => $lang_name), array('data' => $translation), array('data' => l($label, 'admin/build/menu/menutranslation/'. $mid .'/'. $lang)));
  }
  return theme('table', $header, $rows);
}

function _menutranslation_get_translation($mid,$lang){
  $sql =  "SELECT title FROM {menutranslation} WHERE mid = '%d' AND lang = '%s'";
  $result = db_fetch_array(db_query($sql, $mid, $lang));
  $translation = ($result['title'] != NULL)?$result['title']:t('n/a');
  return $translation;
}

function _menutranslation_translate($mid, $lang) {
  $sql_title =  "SELECT title FROM {menu} WHERE mid = '%d'";
  $result = db_fetch_array(db_query($sql_title, $mid));
  drupal_set_breadcrumb(array(l(t('Home'), NULL), l(t('Administer'), 'admin'), l(t('Menutranslation'), 'admin/build/menu/menutranslation'), l(t('Translation for menu item @item', array('@item' => $result['title'])), 'admin/build/menu/menutranslation/'. $mid))); 
  $lang_arr = _i18n_locale_supported_languages();
  drupal_set_title($lang_arr[$lang] .' translation:'); 
  return drupal_get_form('menutranslation_form', $mid, $lang);  
}

function menutranslation_form($mid, $lang) {
   $sql =  "SELECT title FROM {menutranslation} WHERE mid = '%d' AND lang = '%s'";
   $result = db_fetch_array(db_query($sql, $mid, $lang));
   $mode = ($result)?'update':'insert';
   $form['translation'] = array(
    '#type' => 'textfield',
    '#title' => t('Translation'),
    '#default_value' => $result['title'],
    '#size' => 60,
    '#maxlength' => 64,
    '#description' => t('Specify the translation for this menu item'),
  );
  $form['mid'] = array('#type' => 'hidden', '#value' => $mid);
  $form['lang'] = array('#type' => 'hidden', '#value' => $lang);
  $form['mode'] = array('#type' => 'hidden', '#value' => $mode);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}


function menutranslation_form_submit($form_id, $form_values) {
  if ($form_values['mode'] == 'insert') {
   db_query("INSERT INTO {menutranslation} (mid, lang, title) VALUES (%d, '%s', '%s')", $form_values['mid'], $form_values['lang'],  $form_values['translation']);    
  }
  elseif ($form_values['mode'] == 'update') {
   db_query("UPDATE {menutranslation} SET title = '%s' WHERE mid = %d AND lang = '%s'", $form_values['translation'], $form_values['mid'], $form_values['lang']);     
  }
  cache_clear_all(NULL, 'cache_menu');
  drupal_goto('admin/build/menu/menutranslation/'. $form_values['mid']);  
}

function menutranslation_translate_menus() {
  global $_menu;
  global $user;
  global $locale;
  $cid = "$user->uid:$locale";
  cache_clear_all($cid, 'cache_menu');
  foreach ($_menu['items'] as $mid => $item) {
    if (($item['type'])) {
      $sql =  "SELECT title FROM {menutranslation} WHERE mid = '%d' AND lang = '%s'";
      $result = db_fetch_array( db_query( $sql, $mid, $locale ) );
      $title = $result['title'];
      if ($title) {
        $_menu['items'][$mid]['title'] = $title; 
      }             
    }
  }
  cache_set($cid, 'cache_menu', serialize($_menu), time() + (60 * 60 * 24));
}