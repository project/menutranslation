
Description:
------------
This module provides a user friendly interface to translate menu items (requires i18n module).
From an end-user point of view, translating menu items is not really intuitive, and comes with some problems:
 - Users have to remember the menu item string, use the 'Manage strings' tab in Localization, fill in the exact string (case sensitive), etc...
 - If a new menu item is added, it might not even show up in the 'Manage strings' tab because it hasn't been called yet. 
   So users will need to visit a page containing the string first, in order to find it in the Locale system.

A lot of people have already complained about this problem, and sooner or later there will be a good solution built into Drupal.
Until then this module provides you with a more easy-to-use interface to translate your menus easily.

Technical information:
  This module stores the menu translations in a separate table (menutranslation).
  Whenever a menu item is updated, the cache_menu table is refreshed with the new menu item strings.
  Apart from that, the built in menu caching of Drupal will be used, so there will be no performance loss.
   
Installation:
-------------
* Requirements before installation:
- Make sure you have the i18n module installed. 
- Make sure that the i18nmenu module is disabled, as this module takes over the job.
- Make sure that the menuitems in the menus that you want to translate, don't have a language assigned. Make them language independent.

* Installation:
- Enable the menutranslation module in admin/build/modules.
- Configure the module at admin/settings/menutranslation: Here you can specify which menu(s) you want to translate.
- Configure the access control at admin/user/access. 
  You can specify which roles can perform the menu translations, and which roles can administer the settings.
- Go to Administer > Site Building > Menu and press tab 'Menu Translation' (admin/build/menu/menutranslation) to translate the menu(s) you specified.
  The selected menu(s) are shown in tables, following the menu hierarchy, and provide a link to perform the translation.

Issues & Support:
-----------------
Please use the issue tracker at drupal.org to report any issues or ask for support:
http://drupal.org/menutranslation/issues

Credits:
--------
Parts of the code for this module was based on the i18nmenu.module by Jose A. Reyero.
Thanks to Jose for his great work on the i18n module.

Author:
--------
Sven Decabooter <sven@svendecabooter.be>